import pandas as pd
import math
import datetime
import dateutil
import numpy as np
import build_100_model

from sklearn.preprocessing import OneHotEncoder
from scipy.sparse import find

tr_path = './data/train.csv'
te_path = './data/test.csv'
tr_output = './train.tr.txt'
va_output = './train.va.txt'
te_output = './test.te.txt'

real_num_list =  ['date_time','orig_destination_distance','srch_ci','srch_co','hotel_cluster']
drop_list = ['cnt','is_booking']
no_oneHot_list = drop_list + real_num_list

start_date = '2014-07-01 00:00:00' # the model build from 2014-7-1
sample_frac = 1. # the fraction of sampling

class MyOneHotEncoder:
    def __init__(self,data):
        self.n_values_ =  [] # Maximum number of values per feature.
        self.feature_indices_ = [] # Indices to feature ranges. 

        max_list =  data.max()
        max_list += 1 # ID start form 0. Thus number of that dimension is max+1
        self.n_values_ = max_list.values.tolist()

        n = 0
        self.feature_indices_.append(n) # Init
        for i in self.n_values_:
            self.feature_indices_.append(n + i)
            n += i

    def transform(self,row):
        row_np = np.array(row,dtype='int')
        return row_np + self.feature_indices_[:-1]

def daydelta(date):
    
    if pd.isnull(date):
        return 0
    parser = dateutil.parser
    try:
        date = parser.parse(date) # become datetime format
    except ValueError:
        return 0
    return (date.day - 365.) / 730. # normalize
    '''
    origin = datetime.datetime(2013,1,1) # origin date is 2013-1-1
    delta = (date-origin).days # daydelta
    return delta
    '''

def convert_dis(dis):
    if pd.isnull(dis): # fill zero if nan
        return 0
    else:
        return int(dis)

def get_column_id(data,drop_l):
    id_list = []
    for i in drop_l:
        id_list.append(data.columns.get_loc(i))

    return id_list

### nomarlize data by minus mean then divided by std
def nomarlize_data(data,column_list):
    for col in column_list:
        data[col] = (data[col] - data[col].mean()) / data[col].std()

def write_fmm_format(data,enc,output,isTest=False):

    ### open output file
    f_out = open(output,'w')

    shape = list(data.shape)
    if isTest:
        shape[1] += 1

    real_num_list_col = get_column_id(data,real_num_list[:-1]) # get a list of real_num_list column id except hotel_cluster
    orig_dis_col = data.columns.get_loc('orig_destination_distance')
    srch_cio_col = get_column_id(data,['srch_ci','srch_co'])
    
    for i in range(shape[0]):
        row = data.iloc[i] # get one row from data
        if not isTest:
            row_one_hot = row.drop(real_num_list) # remove the drop_list columns
        else:
            row_one_hot = row.drop(real_num_list[:-1]) # remove the drop_list columns
        enc_trans = enc.transform(row_one_hot) # return np array list of field numbers
        
        if not isTest:
            f_out.write('{} '.format(int(row[-1]))) # write hotel_cluster
        else:
            f_out.write('0 ')

        enc_i = 0
        for j in range(shape[1]-1): # except the last column which is hotel_cluster
            ### wirte the real number fields
            if j in real_num_list_col:
                if j == orig_dis_col and pd.isnull(row[j]): # skip orig_dis column if nan
                    continue
                elif j in srch_cio_col and row[j] == 0: # skip srch_ci and srch_co column if nan
                    continue
                f_out.write('{}:{}:{} '.format(j,enc.feature_indices_[-1] + real_num_list_col.index(j),row[j]))

            ### write the one-hot fields
            else:
                if isTest:
                    if enc_trans[enc_i] > enc.feature_indices_[enc_i+1]:
                        enc_i += 1
                        continue
                f_out.write('{}:{}:{} '.format(j,enc_trans[enc_i],1))
                enc_i += 1

        f_out.write('\n')
    f_out.close()



def build(train=tr_path,test=te_path,build_test=False):

    ### read the csv file
    data = pd.read_csv(train)

    ''' DEPRECATED
    ### build one-hot
    enc = OneHotEncoder()
    enc.fit( data.drop(drop_list,axis=1) ) # only make one-hot for IDs
    print enc.n_values_
    print enc.feature_indices_
    '''
    
    ### Do one hot encoding
    enc = MyOneHotEncoder( data.drop(no_oneHot_list,axis=1)) # put the data without some columns
    print enc.n_values_
    print len(enc.n_values_)
    print enc.feature_indices_
    print len(enc.feature_indices_)


    ### trim data to the date_time start from start date
    data = data[ data['date_time'] > start_date ]
    data = data.drop(drop_list,axis=1)

    ### trim data size to 1/4
    if sample_frac != 1.:
        data = data.sample(frac=sample_frac)

    ### change date to daydelta
#    data['date_time'] = data['date_time'].apply(daydelta)
#    data['srch_ci'] = data['srch_ci'].apply(daydelta)
#    data['srch_co'] = data['srch_co'].apply(daydelta)

    ''' DEPRECATED
    ### change dis to int;  fill zero if nan
    data['orig_destination_distance'] = data['orig_destination_distance'].apply(convert_dis)
    '''

    ### normalize the real number fields
    nomarlize_data(data,['orig_destination_distance']) # normalize distance

    ### write to output
#    write_fmm_format(data,enc,tr_output)

    ### if need to build test
    if build_test:

        ### get the mean & std from training data to apply on test
        norm = []
        norm.append(data['orig_destination_distance'].mean())
        norm.append(data['orig_destination_distance'].std())

        del data
        test_data = pd.read_csv(te_path)

        # drop id column
        test_data = test_data.drop('id',axis=1)

        ### change date to daydelta
        test_data['date_time'] = test_data['date_time'].apply(daydelta)
        test_data['srch_ci'] = test_data['srch_ci'].apply(daydelta)
        test_data['srch_co'] = test_data['srch_co'].apply(daydelta)

        ### normalize data
        test_data['orig_destination_distance'] = (test_data['orig_destination_distance'] - norm[0]) / norm[1]

        ### write to output
        write_fmm_format(test_data,enc,te_output,isTest=True)

    ### build 100 fmm input format for 100 fmm-train
#    build_100(tr_output)


if __name__== '__main__':
    build(build_test=True)
