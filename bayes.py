import pandas as pd
import random
import numpy as np

def map5(df, prob_matrix):
    score = 0.
    for idx, row in df.iterrows():
        lab = row.hotel_cluster
        srch_id = row.srch_destination_id
        for rank, hotel_id in enumerate(prob_matrix[srch_id].argsort()[::-1][:5]):
            if hotel_id + 1 == lab:
                score += 1 / (rank + 1)
    return score / len(df)

def mk_prob_matrix(df):
    max_srch_id = 65107
    num_hotel_cluster = 100
    #weights of book to click
    weights = (1, 8)
    alpha = 0.2
    groups = df.groupby(["srch_destination_id", "hotel_cluster"])
    prob_matrix = np.zeros((max_srch_id, num_hotel_cluster))
    prev_x = 0
    N_samples = 0
    for (x, y), group in groups:
        if x != prev_x:
            prob_matrix[prev_x - 1] = (prob_matrix[prev_x - 1] + alpha) / (N_samples + max_srch_id * alpha)
            for i in range(num_hotel_cluster):
                if prob_matrix[prev_x - 1][i] == 0:
                    prob_matrix[prev_x - 1][i] = alpha / (N_samples + max_srch_id * alpha)
            N_samples = 0
        clicks = len(group.is_booking[group.is_booking == False])
        bookings = len(group.is_booking[group.is_booking == True])
        N_samples += clicks * 1 + bookings * 8
        prob_matrix[x - 1][y - 1] = clicks * 1 + bookings * 8
        prev_x = x

    np.savetxt("srchid_bayes.mdl", prob_matrix)

train = pd.read_csv("../../expedia_2016/train.csv")
train["date_time"] = pd.to_datetime(train["date_time"])
train["year"] = train["date_time"].dt.year
train["month"] = train["date_time"].dt.month
t1 = train[((train.year == 2014) & (train.month >= 8))]
t2 = train[((train.year == 2013) | ((train.year == 2014) & (train.month < 8)))]
t2 = t2[t2.is_booking==True]
prob_matrix = np.loadtxt("srchid_bayes.mdl")
print map5(t2, prob_matrix)
