from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import numpy as np
import pickle
import os
import math


dirs = '/tmp3/linyunhsuan'
data_path = os.path.join(dirs, 'data')
ans_path = os.path.join(dirs, 'answer')
model_path = os.path.join(dirs, 'RandomForestClassifier_big.pkl')
def readTrain(csv_filename):
	# str_col = [0, 11, 12]
	train_csv = pd.read_csv(csv_filename)
	train_csv = train_csv.fillna(0)
	train = np.array(train_csv)
	# train = train[:10000]	
	str_col = [idx for idx in range(train.shape[1]) if (type(train[0][idx]) is not float) and (type(train[0][idx]) is not int)]
	for col_idx in str_col:
		if col_idx == 0:
			train[:, col_idx] = np.array([x.split()[0] for x in train[:, col_idx]])
		
		train[:, col_idx] = np.array([int(x.split('-')[1]) if type(x) is str else 0 for x in train[:, col_idx]])
	
	# train_cluster = np.copy(train[:, -1])
	# del_row = []
	# for idx in range(len(train_cluster)):
	# 	if type(train_cluster[idx]) is not int:
	# 		del_row.append(idx)
	del_row = [idx for idx in range(train.shape[0]) if type(train[idx][23]) is not int]
	print(del_row)
	train = np.delete(train, del_row, 0)

	train_x = np.delete(train, [18, 19, 23], 1)
	train_isbooking = np.copy(train[:, 18])
	train_cnt = np.copy(train[:, 19])
	train_cluster = np.copy(train[:, -1])
	return train_x, train_isbooking, train_cnt, train_cluster

def readTest(csv_filename):
	test_csv = pd.read_csv(csv_filename)
	test_csv = test_csv.fillna(0)
	test = np.array(test_csv)
	# test = test[:5000]
	str_col = [idx for idx in range(test.shape[1]) if (type(test[0][idx]) is not float) and (type(test[0][idx]) is not int)]
	for col_idx in str_col:
		if col_idx == 0:
			test[:, col_idx] = np.array([x.split()[0] for x in test[:, col_idx]])
		
		test[:, col_idx] = np.array([int(x.split('-')[1]) if type(x) is str else 0 for x in test[:, col_idx]])
		
	test_x = np.delete(test, 0, 1)
	return test_x
print("start loading training data...")
train_x, train_isbooking, train_cnt, train_cluster = readTrain(os.path.join(data_path, 'train.csv'))
# print(train_x.shape)
# print(train_cluster.shape)

print("start loading testing data...")
test_x = readTest(os.path.join(data_path, 'test.csv'))
clf = RandomForestClassifier(n_estimators=1000, max_depth=5)
# clf = MultinomialNB()
print('building model...')
clf.fit(train_x, train_cluster)

with open(model_path) as file:
	pickle.dump(clf, file)

print('calculateing answer...')
answer = clf.predict_log_proba(test_x)

rank = np.argsort(-answer)[:, :5]

fout = open(ans_path + '_baseline_big.csv', 'w')
for idx, line in rank:
	fout.write(str(idx) + ',')
	for ans in line:
		fout.write(str(ans + ' '))
	fout.write('\n')
fout.close()
with open(ans_path + '_big.pickle', 'wb') as file:
	pickle.dump(answer, file)



fout = open(ans_path + '_big.log', 'w')
fout.truncate()
for row in answer:
	for prob in row:
		fout.write(str(prob) + ' ')
	fout.write('\n')	

fout.close()