import os
import os.path
import subprocess
import random

from random import shuffle
from os import listdir
from os.path import isfile, join

raw_model = './back/after7/train.tr.txt'
train_path = './train_2'
valid_path = './valid_2'
name = './model/train.tr_'
ffm_tr = './libffm/ffm-train'
ffm_model = './ffm_model_2/'

trim_n = 0.05
def build_100_fmm():
    ffm_input_files = [f for f in listdir(model_path) if isfile(join(model_path, f))]
    for f in ffm_input_files:
        cmd = ffm_tr + ' -s 8 ' + join(model_path,f) + ' ' + join(ffm_model,f[:-4]) + '.model'
        print cmd
        process = subprocess.Popen(cmd, shell=True)
        process.wait()


def build_100(raw = raw_model):
    if not os.path.exists(train_path):
        os.makedirs(train_path)
    if not os.path.exists(valid_path):
        os.makedirs(valid_path)
            
    line = []
    print 'Read raw model...'
    with open(raw,'r') as f:
        line = f.read().splitlines()
    f.close()

    for i in range(100):
        out_line = []
        n = 0
        n1 = 0
        n0 = 0
        for l in line:
            if i == int(l.split()[0]):
                out_line.append('1 '+l[l.find(" ")+1:])
                n1 += 1
            else:
                if random.random() < trim_n:
                    n0 += 1
                    out_line.append('0 '+l[l.find(" ")+1:])
        print n0
        print n1
        shuffle(out_line)
        
        print "Build model {}".format(i)
        length = float(len(out_line))
        print "  make training set"

        f_o = open(join(train_path,'train.tr_'+str(i)),'w')
        for j in range(int(length*0.9)):
            f_o.write(out_line[j]+'\n')

        print "  make validation set"
        f_o = open(join(valid_path,'train.va_'+str(i)),'w')
        for j in range(int(length*0.9),int(length)):
            f_o.write(out_line[j] + '\n')



        
#    build_100_ffm()

if __name__ == "__main__":
    build_100()
