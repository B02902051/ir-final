import numpy as np
from scipy.sparse import csr_matrix, coo_matrix
from sklearn.decomposition import PCA, TruncatedSVD
from sklearn.ensemble import AdaBoostClassifier
import pickle
import os
label=[]
global train_n, test_n

def to_sparse(path1,path2):
    col=[]
    row=[]
    data=[]
    global test_n, train_n
    with open(path1, 'r+') as f:
        count = 0
        for line in f:
            temp = line.split()
            label.append(int(temp[0]))
            for i in range(1,len(temp)):
                c,v=temp[i].split(':')    
                row.append(count)
                col.append(int(c))
                data.append(float(v.strip()))
            count += 1
            #if count > 100000:
            #   break
        train_n = count
    with open(path2, 'r+') as f:
        for line in f:
            temp = line.split()
            for i in range(1,len(temp)):
                c,v=temp[i].split(':')
                row.append(count)
                col.append(int(c))
                data.append(float(v.strip()))
            count += 1
        test_n = count - train_n
    coo = coo_matrix((data, (row, col)))
    return csr_matrix(coo)

def get_size(path1, path2):
    global train_n, test_n, label
    count = 0
    with open(path1, 'r+') as f:
        for line in f:
            label.append(int(line.split()[0]))
            count += 1
    train_n = count
    count = 0
    with open(path2, 'r+') as f:
        for line in f:
            count += 1
    test_n = count

if __name__ == '__main__':
    p='/tmp2/b02902026/train.tr.txt'
    p2='/tmp2/b02902026/test.txt'
    if os.path.exists('/tmp2/b02902026/SVD.pickle'):
        print 'load pickle'
        f = open('/tmp2/b02902026/SVD.pickle', 'r+')
        low_dim = pickle.load(f)
        f.close()
        get_size(p,p2)
        print train_n, test_n
    else:
        print 'no pickle'
        mat=to_sparse(p,p2)
        svd=TruncatedSVD(n_components=30)
        low_dim=svd.fit_transform(mat)
        f = open('/tmp2/b02902026/SVD.pickle', 'w+')
        pickle.dump(low_dim,f)
        f.close()
    
    print 'Finish SVD'
    train = low_dim[:train_n]
    test = low_dim[train_n:]
    model = AdaBoostClassifier(n_estimators=30)
    model.fit(train, label)
    f2=open('/tmp2/b02902026/model.pickle', 'w+')
    pickle.dump(model, f2)
    f2.close()
    pred = model.predict_log_proba(test)
    clist = model.classes_
    ans = []
    for i in range(test_n):
        s = np.argsort(pred[i])
        #new = np.sort(pred[i])
        Idx = len(s) - 1
        print "%d,%d %d %d %d %d"%(i, clist[Idx], clist[Idx-1], clist[Idx-2],clist[Idx-3], clist[Idx-4])
        

