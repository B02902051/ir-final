import os.path
import numpy as np

from os import listdir
from os.path import isfile, join



ffm_pr_path = "ffm_predict/"
ffm_in = 'test_'
out = "ffm.csv"

def ffm_predict():
    ffm_input_files = [f for f in listdir(ffm_pr_path) if isfile(join(ffm_pr_path, f))]
    
    result = np.array([])
    for k in range(100):
        with open(join(ffm_pr_path,ffm_in + str(k) + '.out')) as f:
            tmp = np.loadtxt(f)
            print "read "+ffm_in+str(k)+'.out'
            if result.size == 0:
                result = tmp
            result = np.vstack((result,tmp))


    top5 = np.argsort(result,axis=0)[-5:][::-1]

    with open(out,'w') as f:
        f.write('id,hotel_cluster\n')
        for i in range(top5.shape[1]):
            f.write('{},{} {} {} {} {}\n'.format(i,top5[0][i],top5[1][i],top5[2][i],top5[3][i],top5[4][i]))



if __name__ == "__main__":
    ffm_predict()
