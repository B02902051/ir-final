import pandas as pd
import math
import datetime
import dateutil
import numpy as np

from sklearn.preprocessing import OneHotEncoder
from scipy.sparse import find

path = '/tmp2/expedia_2016/train.csv'
tr_output = '../../expedia_2016/train.tr_book.txt'
va_output = '../../expedia_2016/train.va.txt'

real_num_list =  ['date_time','orig_destination_distance','srch_ci','srch_co','hotel_cluster']
drop_list = ['cnt','is_booking']
no_oneHot_list = drop_list + real_num_list

start_date = '2014-07-01 00:00:00' # the model build from 2014-7-1

class MyOneHotEncoder:
    def __init__(self,data):
        self.n_values_ =  [] # Maximum number of values per feature.
        self.feature_indices_ = [] # Indices to feature ranges. 

        max_list =  data.max()
        max_list += 1 # ID start form 0. Thus number of that dimension is max+1
        self.n_values_ = max_list.values.tolist()

        n = 0
        self.feature_indices_.append(n) # Init
        for i in self.n_values_:
            self.feature_indices_.append(n + i)
            n += i

    def transform(self,row):
        row_np = np.array(row)
        return row_np + self.feature_indices_[:-1]

def daydelta(date):
    
    if pd.isnull(date):
        return 0
    parser = dateutil.parser
    date = parser.parse(date) # become datetime format
    origin = datetime.datetime(2013,1,1) # origin date is 2013-1-1
    delta = (date-origin).days # daydelta
    return delta


def convert_dis(dis):
    if pd.isnull(dis): # fill zero if nan
        return 0
    else:
        return int(dis)

def get_column_id(data,drop_l):
    id_list = []
    for i in drop_l:
        id_list.append(data.columns.get_loc(i))

    return id_list


def build(f=path):

    ### read the csv file
    data = pd.read_csv(f)
    ### open output file
    f_out = open(tr_output,'w')



    ''' DEPRECATED
    ### build one-hot
    enc = OneHotEncoder()
    enc.fit( data.drop(drop_list,axis=1) ) # only make one-hot for IDs
    print enc.n_values_
    print enc.feature_indices_
    '''
    
    ### Do one hot encoding
    enc = MyOneHotEncoder( data.drop(no_oneHot_list,axis=1)) # put the data without some columns
    #print enc.n_values_
    #print len(enc.n_values_)
    #print enc.feature_indices_
    #print len(enc.feature_indices_)

    

    ### trim data to the date_time start from start date
    data = data[ data['date_time'] > start_date ]
    data = data[data['is_booking'] == 1]
    #is_booking = list(data['is_booking'])
    data = data.drop(drop_list,axis=1)

    ### change date to daydelta
    data['date_time'] = data['date_time'].apply(daydelta)
    data['srch_ci'] = data['srch_ci'].apply(daydelta)
    data['srch_co'] = data['srch_co'].apply(daydelta)
    ### change dis to int;  fill zero if nan
#    data['orig_destination_distance'] = data['orig_destination_distance'].apply(convert_dis)

    shape = data.shape
    real_num_list_col = get_column_id(data,real_num_list) # get a list of real_num_list column id
    orig_dis_col = data.columns.get_loc('orig_destination_distance')
    srch_cio_col = get_column_id(data,['srch_ci','srch_co'])
    
    ### write to output
    for i in range(shape[0]):
        row = data.iloc[i] # get one row from data
        row_one_hot = row.drop(real_num_list) # remove the drop_list columns
        enc_trans = enc.transform(row_one_hot) # return np array list of field numbers
        iteration = 0
        """
        if is_booking[i] == 1:
            iteration = 6
        else:
            iteration = 1
        """
        f_out.write('{} '.format(int(row[-1]))) # write hotel_cluster

        enc_i = 0
        for j in range(shape[1]-1): # except the last column which is hotel_cluster
            ### wirte the real number fields
            if j in real_num_list_col:
                if j == orig_dis_col and pd.isnull(row[j]): # skip orig_dis column if nan
                    continue
                elif j in srch_cio_col and row[j] == 0: # skip srch_ci and srch_co column if nan
                    continue
                f_out.write('{}:{} '.format(int(enc.feature_indices_[-1] + real_num_list_col.index(j)),row[j]))

            ### write the one-hot fields
            else:
                f_out.write('{}:{} '.format(int(enc_trans[enc_i]),1))
                enc_i += 1

        f_out.write('\n')
    f_out.close()
    

if __name__== '__main__':
    build()
